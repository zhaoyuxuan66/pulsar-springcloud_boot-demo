package com.seu.pulsar;

import com.seu.pulsar.listener.AuditReplyResultListener;
import org.apache.pulsar.client.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class PulsarUtils {

    @Autowired
    PulsarProperties pulsarProperties;

    @Autowired
    PulsarClient client;

    @Autowired
    AuditReplyResultListener<String> auditReplyResultListener;

    /**
     * 创建一个生产者
     *
     * @param topic topic name
     * @return Producer生产者
     */
    public Producer<byte[]> createProducer(String topic) {

        try {
            return client.newProducer()
                    .topic(pulsarProperties.getCluster() + "/" + pulsarProperties.getTdcNamespace() + "/" + topic)
                    .batchingMaxPublishDelay(10, TimeUnit.MILLISECONDS)
                    .sendTimeout(10, TimeUnit.SECONDS)
                    .blockIfQueueFull(true)
                    .create();
        } catch (PulsarClientException e) {
        }

        throw new RuntimeException("初始化Pulsar Producer失败");
    }

    /**
     * 创建一个消费者
     *
     * @param topic           topic name
     * @param subscription    sub name
     * @param messageListener MessageListener的自定义实现类
     * @return Consumer消费者
     */
    public Consumer createConsumer(String topic, String subscription,
                                   MessageListener messageListener) {
        try {
            return client.newConsumer()
                    .topic(pulsarProperties.getCluster() + "/" + pulsarProperties.getTdcNamespace() + "/" + topic)
                    .subscriptionName(subscription)
                    .ackTimeout(10, TimeUnit.SECONDS)
                    .subscriptionType(SubscriptionType.Shared)
                    .messageListener(messageListener)
                    .subscribe()
                    ;
        } catch (PulsarClientException e) {
        }

        throw new RuntimeException("初始化Pulsar Consumer失败");
    }

    /**
     * 异步send一条msg
     *
     * @param message 消息体
     */
    public void sendMessage(String message, Producer<byte[]> producer) {
        producer.sendAsync(message.getBytes()).thenAccept(msgId -> {
        });
    }

    /**
     * 同步发送一条msg
     *
     * @param message  消息体
     * @param producer 生产者实例
     */
    public void sendOnce(String message, Producer<byte[]> producer) throws PulsarClientException {
        MessageId send = producer.send(message.getBytes());
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println(send);
    }

    //-----------consumer-----------

    @Bean(name = "audit-reply-result-topic")
    public Consumer getAuditReplyResultTopicConsumer() {
        return this.createConsumer(pulsarProperties.getTopicMap().get("audit-reply-result-topic"),
                pulsarProperties.getSubMap().get("resource-sub-audit-reply-result"),
                auditReplyResultListener);
    }

    @Bean(name = "reply-publish-topic22")
    public Consumer getReply() {
        return this.createConsumer(pulsarProperties.getTopicMap().get("reply-publish-topic"),
                pulsarProperties.getSubMap().get("audit-sub-reply-publish"),
                auditReplyResultListener);
    }

    //-----------producer-----------
    @Bean(name = "resource-publish-topic")
    public Producer<byte[]> getResourcePublishTopicProducer() {
        return this.createProducer(pulsarProperties.getTopicMap().get("resource-publish-topic"));
    }

    @Bean(name = "comment-publish-topic")
    public Producer<byte[]> getCommentPublishTopicProducer() {
        return this.createProducer(pulsarProperties.getTopicMap().get("comment-publish-topic"));
    }

    @Bean(name = "reply-publish-topic")
    public Producer<byte[]> getReplyPublishTopicProducer() {
        return this.createProducer(pulsarProperties.getTopicMap().get("reply-publish-topic"));
    }
}

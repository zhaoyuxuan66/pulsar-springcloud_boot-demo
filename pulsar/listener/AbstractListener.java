package com.seu.pulsar.listener;

import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.Message;
import org.apache.pulsar.client.api.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractListener<String> implements MessageListener<String> {


    @Override
    public void received(Consumer<String> consumer, Message<String> message) {

    }

}

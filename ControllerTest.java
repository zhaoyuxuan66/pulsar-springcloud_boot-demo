package com.seu;

import com.seu.pulsar.PulsarUtils;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class ControllerTest {

    @Autowired
    PulsarUtils pulsarUtils;

    @Qualifier("reply-publish-topic")
    @Autowired
    Producer<byte[]> producer;

    @Scheduled(cron = "0/4 * * * * ?")
    public void configureTasks() {
        try {
          pulsarUtils.sendOnce("test",producer);
        } catch (PulsarClientException e) {
            e.printStackTrace();
        }
    }
}

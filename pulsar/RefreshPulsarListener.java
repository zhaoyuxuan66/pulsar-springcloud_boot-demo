package com.seu.pulsar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class RefreshPulsarListener implements ApplicationListener {

    @Autowired
    ApplicationContext applicationContext;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {

        if (event.getSource().equals("__refreshAll__")) {
//            log.info("Nacos配置中心配置修改 重启Pulsar====================================");
//            log.info("重启PulsarClient,{}", applicationContext.getBean("getPulsarClient"));
//            log.info("重启PulsarConsumer,{}", applicationContext.getBean("audit-resource-result-topic"));
//            log.info("重启PulsarConsumer,{}", applicationContext.getBean("audit-comment-result-topic"));
//            log.info("重启PulsarConsumer,{}", applicationContext.getBean("audit-reply-result-topic"));
        }
    }

}

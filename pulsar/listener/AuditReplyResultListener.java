package com.seu.pulsar.listener;

import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuditReplyResultListener<String> extends AbstractListener<String> {

    @Override
    public void received(Consumer consumer, Message msg) {
        try {
            System.out.println(msg.getData());
            consumer.acknowledge(msg);
        } catch (Exception e) {
            consumer.negativeAcknowledge(msg);
        }
    }
}
